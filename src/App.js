import "./sass/sass.scss";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Categories from "./HomePages/Categories/Categories";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes></Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
